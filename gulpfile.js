var gulp = require('gulp');
var uglify = require('gulp-uglify');


gulp.task('default', function () {
    gulp.start(
        'js',
        'css',
        'template'
    );
});

gulp.task('js', function () {
    gulp.src(['./node_modules/angular/*.js']).pipe(gulp.dest('./public/js/angular'));
    gulp.src(['./node_modules/ui-select/dist/*.js']).pipe(gulp.dest('./public/js/ui-select'));
    gulp.src(['./node_modules/angular-touch/*']).pipe(gulp.dest('./public/js/angular-touch'));
    gulp.src(['./node_modules/jquery/dist/jquery.min.js']).pipe(gulp.dest('./public/js/jquery'));
    gulp.src(['./node_modules/bootstrap/dist/js/*.js']).pipe(gulp.dest('./public/js/bootstrap'));
    gulp.src(['./node_modules/angular-animate/*']).pipe(gulp.dest('./public/js/angular-animate'));
    gulp.src(['./node_modules/angular-resource/*.js']).pipe(gulp.dest('./public/js/angular-resource'));
    gulp.src(['./node_modules/angular-sanitize/*.js']).pipe(gulp.dest('./public/js/angular-sanitize'));
    gulp.src(['./node_modules/angular-ui-router/release/*.js']).pipe(gulp.dest('./public/js/angular-ui-router'));
    gulp.src(['./node_modules/angular-ui-bootstrap/dist/*.js']).pipe(gulp.dest('./public/js/angular-ui-bootstrap'));
});

gulp.task('css', function () {
    gulp.src(['./node_modules/bootstrap/dist/css/*.css']).pipe(gulp.dest('./public/css/bootstrap'));
    gulp.src(['./node_modules/ui-select/dist/*.css']).pipe(gulp.dest('./public/css/ui-select'));
});

gulp.task('template', function () {
    gulp.src(['./node_modules/angular-ui-bootstrap/template/**/*']).pipe(gulp.dest('./public/templates/bootstrap'));
});

