noteApp.controller("indexController", ['$scope', 'noteResource', function ($scope, noteResource) {

    $scope.notes = null;

    $scope.listParameters = {
        page: 1,
        limit: 10,
        remembered: false,
        orderBy: 'createdAt',
        sortDirection: 'ASC'
    };

    $scope.limitFilterOptions = [
        {value: 10, name: '10'},
        {value: 25, name: '25'},
        {value: 50, name: '50'}
    ];

    $scope.search = '';

    $scope.notesTypeFilterOptions = [
        {value: true, name: 'Remembered'},
        {value: false, name: 'Not remembered'}
    ];

    $scope.getNewData = function () {
        getNotesData();
    };

    $scope.changeOrder = function (newOrderBy) {
        $scope.listParameters.orderBy = newOrderBy;
        getNotesData();
    };


    function getNotesData() {
        noteResource.query($scope.listParameters, function (data) {
            $scope.notes = data.results;
            $scope.listParameters = data.metadata;
        })
    }

    getNotesData();
}]);