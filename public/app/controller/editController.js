noteApp.controller("editController", ['$scope', '$state', '$stateParams', 'noteResource', 'alertService', function ($scope, $state, $stateParams, noteResource, alertService) {

    $scope.note = null;

    noteResource.get({id: $stateParams.id}, function (data) {
        $scope.note = data;
    });

    $scope.update = function () {
        noteResource.update({id: $scope.note.id}, $scope.note, function (response) {
            $state.go('show', {id: response.id});
            alertService.add('success', ['Note ' + response.title + ' updated successfully']);
        }, function (response) {
            if (response.data.errors.length) {
                alertService.add('errors', response.data.errors);
            }
        })
    }
}]);