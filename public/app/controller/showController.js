noteApp.controller("showController", ['$scope', '$stateParams', 'noteResource', function ($scope, $stateParams, noteResource) {

    $scope.note = null;

    noteResource.get({id: $stateParams.id}, function (data) {
        $scope.note = data;
    })
}]);