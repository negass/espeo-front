noteApp.controller("addController", ['$scope', '$state', 'noteResource', 'alertService', function ($scope, $state, noteResource, alertService) {

    $scope.note = {
        title: null,
        content: null,
        isRemembered: false
    };

    $scope.addNote = function () {
        noteResource.save($scope.note, function (response) {
            $state.go('show', {id: response.id});
            alertService.add('success', ['Note ' + response.title + ' added successfully']);
        }, function (response) {
            if (response.data.errors.length) {
                alertService.add('errors', response.data.errors);
            }
        });
    }
}]);