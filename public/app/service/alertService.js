noteApp.factory('alertService', function ($rootScope) {

    $rootScope.alerts = {};

    $rootScope.$on('$stateChangeStart', function () {
        $rootScope.alerts = [];
    });

    return {
        add: function (type, messages) {
            $rootScope.alerts[type] = messages;
        }
    };
});