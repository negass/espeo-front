noteApp.factory('noteResource', function ($resource) {

    return $resource('http://api.note.dev:8080/notes/:id', {id: '@id'}, {
        query: {
            method: 'GET',
            isArray: false
        },
        update: {
            method: 'PUT'
        }
    });
});