var noteApp = angular.module("noteApp", ['ui.bootstrap', 'ngResource', 'ngTouch', 'ui.router']);

noteApp.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('index', {
            url: "/",
            templateUrl: "view/partials/index.html"
        })
        .state('add', {
            url: "/add",
            templateUrl: "view/partials/add.html"
        })
        .state('show', {
            url: "/note/{id}",
            templateUrl: "view/partials/show.html"
        })
        .state('edit', {
            url: "/note/{id}/edit",
            templateUrl: "view/partials/edit.html"
        })
});